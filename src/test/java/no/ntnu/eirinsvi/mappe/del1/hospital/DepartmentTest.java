package no.ntnu.eirinsvi.mappe.del1.hospital;

import no.ntnu.eirinsvi.mappe.del1.hospital.exception.RemoveException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * DepartmentTest tests the method remove in Department, to see if it removes the person if it is in the list,
 * and if the exception gets thrown when the person is not
 * @author Eirin
 */

public class DepartmentTest{

    Patient patient = new Patient("name","names","100B");
    Employee employee = new Employee("name", "names", "10A");
    Department heart = new Department("heart");

    @Nested
    class removeMethodWorksAndSendsException {
        @Test
        public void removePersonWhoIsAPatient() {
            heart.addPatient(patient);
            int startLength = heart.getPatients().size();
            try {
                heart.remove(patient);
            } catch (RemoveException e) {
                System.err.println(e.getMessage());
            }
            assertTrue(startLength > heart.getPatients().size());
        }
        @Test
        public void removeRersonWhoIsAnEmployee() {
            heart.addEmployee(employee);
            int startLength = heart.getEmployees().size();
            try {
                heart.remove(employee);
            } catch (RemoveException e) {
                System.err.println(e.getMessage());
            }
            assertTrue(startLength > heart.getEmployees().size());
        }
        @Test
        public void removePersonWhoIsNotInAnyRegistre() {
            assertThrows(RemoveException.class, () -> heart.remove(employee));
        }
    }
}
