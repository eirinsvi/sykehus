package no.ntnu.eirinsvi.mappe.del1.hospital;

import no.ntnu.eirinsvi.mappe.del1.hospital.healthpersonal.doctor.Surgeon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * SurgeonTest tests if members of Surgeon can set a patients diagnosis.
 * @author Eirin
 */
public class SurgeonTest {
    Surgeon surgeon = new Surgeon("Heidi","Antonsen","123");
    Patient patient = new Patient("Sara","Simonsen","456");

    @Test
    public void surgeonCanSetDiagnosisOnPatient() {
        surgeon.setDiagnosis(patient, "Diabetes");
        assertEquals(patient.getDiagnosis(), "Diabetes");
    }
}
