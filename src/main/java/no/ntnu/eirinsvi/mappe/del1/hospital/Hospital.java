package no.ntnu.eirinsvi.mappe.del1.hospital;

import java.util.ArrayList;

/**
 * Hospital holds a list of all the departments. The archive provides
 * functionality for adding departments to the register.
 *
 * @author Eirin
 */

public class Hospital {
    private final String hospitalName;
    ArrayList<Department> departments = new ArrayList<>();

    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public ArrayList<Department> getDepartments(){
        ArrayList<Department> clone = (ArrayList<Department>) departments.clone();
        return clone;
    }

    /**
     * addDepartment adds a new department to the register, if it doesn't alredy exist in the register.
     * If so, an illegal argument exception gets thrown.
     *
     * @throws IllegalArgumentException
     * @param department is the department who is supposed to get added
     */
    public void addDepartment(Department department) throws IllegalArgumentException {
        if (!departments.contains(department)) {
            departments.add(department);
        } else { throw new IllegalArgumentException("Department with that name is already inlisted.");}
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departments=" + departments +
                '}';
    }
}
