package no.ntnu.eirinsvi.mappe.del1.hospital.exception;

/**
 * RemoveException takes in the removeException which gets thrown in the remove method in Department.
 *
 * @author Eirin
 */
public class RemoveException extends Exception{
    private static final long serialVersionUID = 1L;

    public RemoveException(String errorMessage){
        super("Person is not supported: " + errorMessage);
    }
}
