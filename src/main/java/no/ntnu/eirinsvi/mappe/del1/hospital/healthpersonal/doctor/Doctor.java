package no.ntnu.eirinsvi.mappe.del1.hospital.healthpersonal.doctor;

import no.ntnu.eirinsvi.mappe.del1.hospital.Employee;
import no.ntnu.eirinsvi.mappe.del1.hospital.Patient;
/**
 * Abstract class Doctor, which extends Employee.
 * GeneralPractitioner and surgeon extend this class, which makes them the only ones
 * who may give a patient a diagnosis, using the abstract method setDiagnosis.
 *
 * @author Eirin
 */
public abstract class Doctor extends Employee {
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
