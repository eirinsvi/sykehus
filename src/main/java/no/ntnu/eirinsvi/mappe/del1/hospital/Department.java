package no.ntnu.eirinsvi.mappe.del1.hospital;

import no.ntnu.eirinsvi.mappe.del1.hospital.exception.RemoveException;

import java.util.HashMap;
import java.util.Objects;

/**
 * Department holds a list of employees and a list of patients. The archives provides
 * functionality for adding members to each register, as well as remove them.
 *
 * @author Eirin
 */
public class Department {
    private String departmentName;
    HashMap<String, Employee> employees = new HashMap<>();
    HashMap<String, Patient> patients = new HashMap<>();

    public Department(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public HashMap<String, Employee> getEmployees(){ return employees; }

    /**
     * Register new employee to the register. Checks if the employee already is in the register.
     * If the patient already exist in the register, an illegalArgumentException gets thrown.
     *
     * @throws IllegalArgumentException
     * @param employee the employee who is supposed to be added.
     */
    public void addEmployee(Employee employee)  throws IllegalArgumentException {
        if (!employees.containsKey(employee.getSocialSecurityNumber())) {
            employees.put(employee.getSocialSecurityNumber(), employee);
        } else { throw new IllegalArgumentException("Employee with that social security number is already inlisted.");}
    }
    /**
     * List all patients in the register for department.
     */
    public HashMap<String, Patient> getPatients(){
        return patients;
    }

    /**
     * Register new patient to the register. Checks if the patient already is in the register before adding.
     * If the patient already exist in the register, an illegalArgumentException gets thrown.
     *
     * @throws IllegalArgumentException
     * @param patient the patient who is supposed to be added.
     */
    public void addPatient(Patient patient) throws IllegalArgumentException{
        if (!patients.containsKey(patient.getSocialSecurityNumber())) {
            patients.put(patient.getSocialSecurityNumber(), patient);
        } else { throw new IllegalArgumentException("Patient with that social security number is already inlisted.");}
    }

    /**
     * remove method checks if the given person is in the register for either patients or employees.
     * If the person is in one of the registers, the method will check if person is an employee or a patient, and then delete the
     * register. If not, it will throw an exception.
     *
     * @throws RemoveException
     * @param person who is supposed to be removed from the register
     */
    public void remove(Person person) throws RemoveException {
        if(employees.containsKey(person.getSocialSecurityNumber()) || patients.containsKey(person.getSocialSecurityNumber())) {
            if (person instanceof Employee) {
                employees.remove(person.getSocialSecurityNumber(), person);
            } else if (person instanceof Patient) {
                patients.remove(person.getSocialSecurityNumber(), person);
            }
        } else{ throw new RemoveException("Person must be an employee or a patient, and be in the register."); }

    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", employees=" + employees +
                ", patients=" + patients +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName) &&
                Objects.equals(employees, that.employees) &&
                Objects.equals(patients, that.patients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }
}
