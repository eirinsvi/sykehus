package no.ntnu.eirinsvi.mappe.del1.hospital;

import no.ntnu.eirinsvi.mappe.del1.hospital.exception.RemoveException;

/**
 * A simple client program to fill in hospital test data, and check remove method in department for valid and invalid Person.
 *
 * @author Eirin
 */
public class HospitalClient {
    public static void main(String[] args) {
        final Hospital rikshospitalet = new Hospital("rikshospitalet");

        /**
         * Fills the register with some arbitrary members, for testing purposes.
         */
        HospitalTestData.fillRegisterWithTestData(rikshospitalet);

        /**
          Removes an employee from register in department.
         */

        Department emergency = rikshospitalet.getDepartments().get(0);
        Employee chosenEmployee = emergency.getEmployees().get("1");
        int totalEmployeesFirst = emergency.getEmployees().size();
        try {
            emergency.remove(chosenEmployee);
        } catch (RemoveException re) {
            System.err.println(re.getMessage());
        }
        int totalEmployeesAfter = emergency.getEmployees().size();
        if(totalEmployeesFirst>totalEmployeesAfter){
            System.out.println("Successful removal of employee");
        }

        /**
          Try to remove a patient who is not in the register.
         */
        try {
            emergency.remove(new Patient("Jan", "Antonsen", "100"));
        } catch (RemoveException re) {
            System.err.println(re.getMessage());
        }

    }
}

