package no.ntnu.eirinsvi.mappe.del1.hospital.healthpersonal;

import no.ntnu.eirinsvi.mappe.del1.hospital.Employee;

public class Nurse extends Employee {
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Nurse{ name: " +
                getFullName() +
                " social securitynumber: " +
                getSocialSecurityNumber();
    }
}
