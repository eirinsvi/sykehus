package no.ntnu.eirinsvi.mappe.del1.hospital;

/**
 * Patient extends Person. Surgeons and general practitioners
 * can set diagnosis because the class implements diagnosible.
 * @author Eirin
 */

public class Patient extends Person implements Diagnosable{
    private String diagnosis = "";


    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Patient{ name: " +
                getFullName() +
                " social securitynumber: " +
                getSocialSecurityNumber() +
                "diagnosis='" + diagnosis + '\'' +
                '}';
    }

    protected String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
}
