package no.ntnu.eirinsvi.mappe.del1.hospital;

/**
 * Interface Diagnosible gets implemented by Patient, because a patient can have a diagnosis
 *
 * @author Eirin
 */
public interface Diagnosable {
    void setDiagnosis(String diagnosis);
}
