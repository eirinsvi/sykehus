package no.ntnu.eirinsvi.mappe.del1.hospital;

/**
 * Class for employee at the hospital, extends person. Gets extended by doctor and nurse.
 *
 * @author Eirin
 */
public class Employee extends Person{

    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Employee{ name: " +
                getFullName() +
                " social securitynumber: " +
                getSocialSecurityNumber();
    }
}
