package no.ntnu.eirinsvi.mappe.del1.hospital.healthpersonal.doctor;

import no.ntnu.eirinsvi.mappe.del1.hospital.Patient;

/**
 * GeneralPractitioner, which extends Doctor.
 * May give a patient a diagnosis with method setDiagnosis.
 *
 * @author Eirin
 */
public class GeneralPractitioner extends Doctor {
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
    public void setDiagnosis(Patient patient, String diagnosis){
        patient.setDiagnosis(diagnosis);
    }
}
